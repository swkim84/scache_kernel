/*
 * Mutexes: blocking mutual exclusion locks
 *
 * started by Ingo Molnar:
 *
 *  Copyright (C) 2004, 2005, 2006 Red Hat, Inc., Ingo Molnar <mingo@redhat.com>
 *
 * This file contains mutex debugging related internal prototypes, for the
 * !CONFIG_DEBUG_MUTEXES case. Most of them are NOPs:
 */
#ifdef CONFIG_SCACHE
#include <linux/scache.h>
#endif

#define spin_lock_mutex(lock, flags) \
		do { spin_lock(lock); (void)(flags); } while (0)
#define spin_unlock_mutex(lock, flags) \
		do { spin_unlock(lock); (void)(flags); } while (0)
#define mutex_remove_waiter(lock, waiter, ti) \
		__list_del((waiter)->list.prev, (waiter)->list.next)

#ifdef CONFIG_SMP
static inline void mutex_set_owner(struct mutex *lock)
{
#ifdef CONFIG_SCACHE
	unsigned long flags;

	spin_lock_irqsave(&lock->critical_lock, flags);
	if (lock->owner != current) {
		if (lock->critical_cnt > 0 && lock->inherit == false) {
			/*
			 * One or more critical processes are waiting for
			 * this lock to be released, so inherit criticality
			 * to the current process during the execution in
			 * mutex-protected region.
			 */
			scache_inc_and_set_critical_kernel(current, SCACHE_INHERIT_MUTEX);
			lock->inherit = true;
		}
		lock->owner = current;
	}
	spin_unlock_irqrestore(&lock->critical_lock, flags);
#else
	lock->owner = current;
#endif
}

static inline void mutex_clear_owner(struct mutex *lock)
{
#ifdef CONFIG_SCACHE
	spin_lock(&lock->critical_lock);
	if (lock->owner != NULL) {
		if (lock->critical_cnt > 0 && lock->inherit == true) {
			scache_dec_and_clear_critical_kernel(lock->owner, SCACHE_INHERIT_MUTEX);
			lock->inherit = false;
		}
		lock->owner = NULL;
	}
	spin_unlock(&lock->critical_lock);
#else
	lock->owner = NULL;
#endif
}
#else
static inline void mutex_set_owner(struct mutex *lock)
{
}

static inline void mutex_clear_owner(struct mutex *lock)
{
}
#endif

#define debug_mutex_wake_waiter(lock, waiter)		do { } while (0)
#define debug_mutex_free_waiter(waiter)			do { } while (0)
#define debug_mutex_add_waiter(lock, waiter, ti)	do { } while (0)
#define debug_mutex_unlock(lock)			do { } while (0)
#define debug_mutex_init(lock, name, key)		do { } while (0)

static inline void
debug_mutex_lock_common(struct mutex *lock, struct mutex_waiter *waiter)
{
}
