#include <linux/sched.h>
#include <linux/sysfs.h>
#include <linux/kobject.h>
#include <linux/mempool.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/blk_types.h>
#include <linux/device-mapper.h>
#include <linux/buffer_head.h>
#include <linux/jbd2.h>
#include <linux/scache.h>
#include <asm/i387.h>

static int enable_stable_page;
static int enable_clflush;
static int enable_nts;
static int enable_ntl;
static int cache_critical_task;
static int cache_writesync;
static int critical_inherit_kernel;
static int critical_inherit_user;
static int reset_all;
static int enable_resubmit;
static int cancel_destaging;
static unsigned long write_delay;
static unsigned long read_delay;

static atomic_t critical_sector_cnt;

spinlock_t scache_tree_lock;
EXPORT_SYMBOL(scache_tree_lock);
static struct rb_root slow_root = RB_ROOT;
static struct rb_root critical_root = RB_ROOT;

static struct kmem_cache *critical_sector_cache = NULL;
static mempool_t *critical_sector_pool = NULL;

/*
 * *************************************************************************
 * Non-temporal memcpy 
 * *************************************************************************
 * Non-temporal memcpy does the following:
 * (1) use movntq to copy into PM space
 * (2) use sfence to flush the data to memory controller
 * 
 * Compared to regular temporal memcpy, it provides several benefits here:
 * (1) writes to PM bypass the CPU cache, which avoids polluting CPU cache
 * (2) reads from PM still benefit from the CPU cache
 * (3) sfence used for each write guarantees data will be flushed out of buffer
 */

static void nts_memcpy_64bytes_v2(void* to, const void* from, size_t size)
{
	int i;
	unsigned bs = 64;   /* write unit size 8 bytes */

	if (size < bs)
		panic("%s(%d) size (%zu) is smaller than %u\n", __FUNCTION__, __LINE__, size, bs);

	if (((unsigned long) from & 64UL) || ((unsigned long)to & 64UL))
		panic("%s(%d) not aligned\n", __FUNCTION__, __LINE__);

	/* start */
	kernel_fpu_begin();

	/* do the non-temporal mov */
	for (i = 0; i < size; i += bs){
		__asm__ __volatile__ (
				"movdqa (%0), %%xmm0\n"
				"movdqa 16(%0), %%xmm1\n"
				"movdqa 32(%0), %%xmm2\n"
				"movdqa 48(%0), %%xmm3\n"
				"movntdq %%xmm0, (%1)\n"
				"movntdq %%xmm1, 16(%1)\n"
				"movntdq %%xmm2, 32(%1)\n"
				"movntdq %%xmm3, 48(%1)\n"
				:
				: "r" (from), "r" (to)
				: "memory");

		to += bs;
		from += bs;
	}

	/* do sfence to push data out */
	__asm__ __volatile__ (
			" sfence\n" : :
			);

	/* end */
	kernel_fpu_end();

	/*NOTE: we assume it would be multiple units of 64 bytes*/
	if (i != size)
		panic("%s:%s:%d size (%zu) is in multiple units of 64 bytes\n", __FILE__, __FUNCTION__, __LINE__, size);

	return;
}

/* non-temporal store */
void scache_nts_memcpy(void* to, const void* from, size_t size)
{
	if (size < 64){
		panic("no support for nt load smaller than 64 bytes yet\n");
	} else {
		nts_memcpy_64bytes_v2(to, from, size);
	}
}
EXPORT_SYMBOL(scache_nts_memcpy);

static void ntl_memcpy_64bytes(void* to, void* from, size_t size)
{
	int i;
	unsigned bs = 64;   /* write unit size 16 bytes */

	if (size < bs)
		panic("%s(%d) size (%zu) is smaller than %u\n", __FUNCTION__, __LINE__, size, bs);

	if (((unsigned long) from & 64UL) || ((unsigned long)to & 64UL))
		panic("%s(%d) not aligned\n", __FUNCTION__, __LINE__);

	/* start */
	kernel_fpu_begin();

	/* do the non-temporal mov */
	for (i = 0; i < size; i += bs){
		__asm__ __volatile__ (
				"movntdqa (%0), %%xmm0\n"
				"movntdqa 16(%0), %%xmm1\n"
				"movntdqa 32(%0), %%xmm2\n"
				"movntdqa 48(%0), %%xmm3\n"
				"movdqa %%xmm0, (%1)\n"
				"movdqa %%xmm1, 16(%1)\n"
				"movdqa %%xmm2, 32(%1)\n"
				"movdqa %%xmm3, 48(%1)\n"
				:
				: "r" (from), "r" (to)
				: "memory");

		to += bs;
		from += bs;
	}

	/* end */
	kernel_fpu_end();

	/*NOTE: we assume it would be multiple units of 64 bytes (at least 512 bytes)*/
	if (i != size)
		panic("%s:%s:%d size (%zu) is in multiple units of 64 bytes\n", __FILE__, __FUNCTION__, __LINE__, size);

	return;
}

/* non-temporal load */
void scache_ntl_memcpy(void* to, void* from, size_t size)
{
	if (size < 64){
		panic("no support for nt load smaller than 64 bytes yet\n");
	} else {
		ntl_memcpy_64bytes(to, from, size);
	}
}
EXPORT_SYMBOL(scache_ntl_memcpy);

#define SCACHE_POOL_SIZE 131072

#define SCACHE_KMEM_CACHE(__struct, __flags) kmem_cache_create("scache_"#__struct,\
		                sizeof(struct __struct), __alignof__(struct __struct),\
		                (__flags), NULL)

static int __init scache_mempool_init(void)
{
	critical_sector_cache = SCACHE_KMEM_CACHE(critical_sector, 0);
	if (!critical_sector_cache)
		return -ENOMEM;

	critical_sector_pool = mempool_create_slab_pool(SCACHE_POOL_SIZE, critical_sector_cache);
	if (!critical_sector_pool)
		return -ENOMEM;

	return 0;
}

static void __init scache_mempool_free(void)
{
	if (critical_sector_pool)
		mempool_destroy(critical_sector_pool);

	if (critical_sector_cache)
		kmem_cache_destroy(critical_sector_cache);
}

inline struct critical_sector *alloc_critical_sector(gfp_t gfp_mask)
{
	struct critical_sector *cs;

	cs = mempool_alloc(critical_sector_pool, gfp_mask);

	return cs;
}
EXPORT_SYMBOL(alloc_critical_sector);

inline void free_critical_sector(struct critical_sector *cs)
{
	mempool_free(cs, critical_sector_pool);
}
EXPORT_SYMBOL(free_critical_sector);

static void scache_inc_and_set_critical(struct task_struct *tsk, unsigned int flag, int inherit_point)
{
	spin_lock(&tsk->critical_lock);
	tsk->inherit_point |= inherit_point;
	tsk->critical_cnt++;
	tsk->critical_flags |= flag;
	spin_unlock(&tsk->critical_lock);
}

static void scache_dec_and_clear_critical(struct task_struct *tsk, unsigned int flag, int inherit_point)
{
	spin_lock(&tsk->critical_lock);
	tsk->inherit_point &= ~inherit_point;
	tsk->critical_cnt--;
	if (tsk->critical_cnt == 0) {
		tsk->critical_flags &= ~flag;
	}
	BUG_ON(tsk->critical_cnt < 0);
	spin_unlock(&tsk->critical_lock);
}

inline void scache_inc_and_set_critical_kernel(struct task_struct *tsk, int inherit_point)
{
	scache_inc_and_set_critical(tsk, SCACHE_CRITICAL_INHERIT_KERNEL, inherit_point);
}
EXPORT_SYMBOL(scache_inc_and_set_critical_kernel);

inline void scache_dec_and_clear_critical_kernel(struct task_struct *tsk, int inherit_point)
{
	scache_dec_and_clear_critical(tsk, SCACHE_CRITICAL_INHERIT_KERNEL, inherit_point);
}
EXPORT_SYMBOL(scache_dec_and_clear_critical_kernel);

inline void scache_inc_and_set_critical_user(struct task_struct *tsk)
{
	scache_inc_and_set_critical(tsk, SCACHE_CRITICAL_INHERIT_USER, SCACHE_INHERIT_USER);
}

inline void scache_dec_and_clear_critical_user(struct task_struct *tsk)
{
	scache_dec_and_clear_critical(tsk, SCACHE_CRITICAL_INHERIT_USER, SCACHE_INHERIT_USER);
}

static inline void set_critical(struct task_struct *tsk)
{
	spin_lock(&tsk->critical_lock);
	tsk->critical_flags |= SCACHE_CRITICAL;
	spin_unlock(&tsk->critical_lock);
}

static inline void clear_critical(struct task_struct *tsk)
{
	spin_lock(&tsk->critical_lock);
	tsk->critical_flags &= ~SCACHE_CRITICAL;
	spin_unlock(&tsk->critical_lock);
}

int scache_set_critical_flags(struct task_struct *tsk, int val)
{
	int ret = 0;

	switch (val) {
		case -99:
			scache_inc_and_set_critical_user(tsk);
			ret = 1;
			break;
		case -100:
			set_critical(tsk);
			ret = 1;
			break;
		case 99:
			scache_dec_and_clear_critical_user(tsk);
			ret = 1;
			break;
		case 100:
			clear_critical(tsk);
			ret = 1;
			break;
		default:
			break;
	}

	return ret;
}

inline int scache_stable_page_enabled(void)
{
	return enable_stable_page;
}

inline int scache_clflush_enabled(void)
{
	return enable_clflush;
}
EXPORT_SYMBOL(scache_clflush_enabled);

inline int scache_nts_enabled(void)
{
	return enable_nts;
}
EXPORT_SYMBOL(scache_nts_enabled);

inline int scache_ntl_enabled(void)
{
	return enable_ntl;
}
EXPORT_SYMBOL(scache_ntl_enabled);

inline unsigned long scache_write_delay(void)
{
	return write_delay;
}
EXPORT_SYMBOL(scache_write_delay);

inline unsigned long scache_read_delay(void)
{
	return read_delay;
}
EXPORT_SYMBOL(scache_read_delay);

inline unsigned long scache_cancel_destaging(void)
{
	return cancel_destaging;
}
EXPORT_SYMBOL(scache_cancel_destaging);

inline int scache_critical_task_enabled(void)
{
	return cache_critical_task;
}
EXPORT_SYMBOL(scache_critical_task_enabled);

inline int scache_cache_writesync_enabled(void)
{
	return cache_writesync;
}
EXPORT_SYMBOL(scache_cache_writesync_enabled);

int scache_is_critical_task(struct task_struct *tsk)
{
	/* if cache_critical_task isn't enabled, consider all task as critical */
	if (!cache_critical_task)
		return 1;

	return (cache_critical_task && /* task-based */
			((tsk->critical_flags & SCACHE_CRITICAL) ||
			 (critical_inherit_user &&
			  (tsk->critical_flags & SCACHE_CRITICAL_INHERIT_USER)) ||
			 (critical_inherit_kernel &&
			  (tsk->critical_flags & SCACHE_CRITICAL_INHERIT_KERNEL))));
}
EXPORT_SYMBOL(scache_is_critical_task);

int scache_is_critical_sector(struct bio *bio)
{
	unsigned long flags;
	struct critical_sector *cs;
	int is_critical_sector = 0;

	spin_lock_irqsave(&scache_tree_lock, flags);
	if ((cs = scache_rb_find_critical(bio->bi_sector))) {
		is_critical_sector = 1;
		atomic_inc(&critical_sector_cnt);
		scache_rb_del_critical(cs);
	} else
		scache_rb_add(bio);
	spin_unlock_irqrestore(&scache_tree_lock, flags);

	return is_critical_sector;
}
EXPORT_SYMBOL(scache_is_critical_sector);

sector_t scache_get_remapped_sector_reverse(struct bio *bio,
		struct block_device *src_bdev)
{
	sector_t sector_nr = bio->bi_sector;

	if (src_bdev != src_bdev->bd_contains) {
		struct hd_struct *p = src_bdev->bd_part;
		sector_nr -= p->start_sect;
	}

	return sector_nr;
}
EXPORT_SYMBOL(scache_get_remapped_sector_reverse);

int scache_rb_add(struct bio *bio)
{
	struct rb_node **p = &slow_root.rb_node;
	struct rb_node *parent = NULL;
	struct bio *__bio;

	BUG_ON(bio_data_dir(bio) != WRITE);

	while (*p) {
		parent = *p;
		__bio = rb_entry(parent, struct bio, rb_node);

		if (bio->bi_sector < __bio->bi_sector)
			p = &(*p)->rb_left;
		else if (bio->bi_sector > __bio->bi_sector)
			p = &(*p)->rb_right;
		else {
			WARN_ON(1);
			return -1;
		}
	}

	rb_link_node(&bio->rb_node, parent, p);
	rb_insert_color(&bio->rb_node, &slow_root);

	return 0;
}
EXPORT_SYMBOL(scache_rb_add);

void scache_rb_del(struct bio *bio)
{
	BUG_ON(RB_EMPTY_NODE(&bio->rb_node));
	rb_erase(&bio->rb_node, &slow_root);
	RB_CLEAR_NODE(&bio->rb_node);
}
EXPORT_SYMBOL(scache_rb_del);

struct bio *scache_rb_find(sector_t sector_nr)
{
	struct rb_node *n = slow_root.rb_node;
	struct bio *bio;

	while (n) {
		bio = rb_entry(n, struct bio, rb_node);

		BUG_ON(bio == NULL);

		BUG_ON(bio_data_dir(bio) != WRITE);

		if (sector_nr < bio->bi_sector)
			n = n->rb_left;
		else if (sector_nr > bio->bi_sector)
			n = n->rb_right;
		else
			return bio;
	}

	return NULL;
}
EXPORT_SYMBOL(scache_rb_find);

int scache_rb_add_critical(struct critical_sector *cs)
{
	struct rb_node **p = &critical_root.rb_node;
	struct rb_node *parent = NULL;
	struct critical_sector *__cs;

	while (*p) {
		parent = *p;
		__cs = rb_entry(parent, struct critical_sector, rb_node);

		if (cs->sector < __cs->sector)
			p = &(*p)->rb_left;
		else if (cs->sector > __cs->sector)
			p = &(*p)->rb_right;
		else
			return -1;
	}

	rb_link_node(&cs->rb_node, parent, p);
	rb_insert_color(&cs->rb_node, &critical_root);

	return 0;
}
EXPORT_SYMBOL(scache_rb_add_critical);

void scache_rb_del_critical(struct critical_sector *cs)
{
	BUG_ON(RB_EMPTY_NODE(&cs->rb_node));
	rb_erase(&cs->rb_node, &critical_root);
	RB_CLEAR_NODE(&cs->rb_node);
}
EXPORT_SYMBOL(scache_rb_del_critical);

struct critical_sector *scache_rb_find_critical(sector_t sector_nr)
{
	struct rb_node *n = critical_root.rb_node;
	struct critical_sector *cs;

	while (n) {
		cs = rb_entry(n, struct critical_sector, rb_node);

		BUG_ON(cs == NULL);

		if (sector_nr < cs->sector)
			n = n->rb_left;
		else if (sector_nr > cs->sector)
			n = n->rb_right;
		else
			return cs;
	}

	return NULL;
}
EXPORT_SYMBOL(scache_rb_find_critical);

void scache_try_linear_remap(dev_t *bdev_nr, sector_t *sector_nr)
{
	if (MAJOR(*bdev_nr) == 252) {
		if (MINOR(*bdev_nr) == 2) {
			*bdev_nr &= ~MINORMASK;
			*bdev_nr |= 1;
			*sector_nr = *sector_nr + 2048;
		} else if (MINOR(*bdev_nr) == 3) {
			*bdev_nr &= ~MINORMASK;
			*bdev_nr |= 1;
			*sector_nr = *sector_nr + 976773168;
		}
	}
}
EXPORT_SYMBOL(scache_try_linear_remap);

struct mapped_device;
int dm_resubmit_bio(struct bio *, struct mapped_device *);

int scache_resubmit_sector(sector_t sector_nr, dev_t bdev, gfp_t gfp_mask, int resubmit_point)
{
	unsigned long flags;
	int ret;
	struct bio *bio_orig;
	struct mapped_device *md = dm_get_md(bdev);
	struct critical_sector *cs;

	if (!enable_resubmit || !is_dm_md(bdev)) {
		ret = SCACHE_RESUBMIT_FAILED;
		goto out;
	}

	current->resubmit_point = resubmit_point;

	cs = alloc_critical_sector(gfp_mask);

	spin_lock_irqsave(&scache_tree_lock, flags);
	bio_orig = scache_rb_find(sector_nr);
	if (bio_orig) {
		scache_rb_del(bio_orig);
		if (!dm_resubmit_bio(bio_orig, md)) {
			/* 
			 * bio should be cancelled only if resubmit success,
			 * which means there is a free block in cache.
			 */
			if (bio_orig->job_need_cancel) {
				*(bio_orig->job_need_cancel) = true;
			} else
				bio_orig->need_cancel = true;
		}
		ret = SCACHE_RESUBMIT_HIT;
		free_critical_sector(cs);
	} else {
		if (cs) {
			cs->sector = sector_nr;
			if (scache_rb_add_critical(cs))
				free_critical_sector(cs);
			ret = SCACHE_RESUBMIT_MISS;
		} else {
			WARN_ON(!cs);
			ret = SCACHE_RESUBMIT_FAILED;
		}
	}
	spin_unlock_irqrestore(&scache_tree_lock, flags);

	current->resubmit_point = 0;
out:
	if (md)
		dm_put(md);

	return ret;
}
EXPORT_SYMBOL(scache_resubmit_sector);

int scache_resubmit_bh(struct buffer_head *head, gfp_t gfp_mask, int resubmit_point)
{
	sector_t sector_nr = head->b_blocknr * (head->b_size >> 9);
	struct block_device *bdev = head->b_bdev;
	dev_t bdev_nr = bdev->bd_dev;

	if (bdev != bdev->bd_contains) {
		struct hd_struct *p = bdev->bd_part;

		sector_nr += p->start_sect;
		bdev_nr = bdev->bd_contains->bd_dev;
	} else {
		scache_try_linear_remap(&bdev_nr, &sector_nr);
	}

	return scache_resubmit_sector(sector_nr, bdev_nr, gfp_mask, resubmit_point);
}
EXPORT_SYMBOL(scache_resubmit_bh);

int scache_resubmit_page(struct page *page, gfp_t gfp_mask, int resubmit_point)
{
	if (page_has_buffers(page))
		return scache_resubmit_bh(page_buffers(page), gfp_mask, resubmit_point);
	else
		return SCACHE_RESUBMIT_FAILED;
}
EXPORT_SYMBOL(scache_resubmit_page);

void scache_resubmit_sector_cleanup(sector_t sector_nr)
{
	struct critical_sector *cs;
	unsigned long flags;

	if (!enable_resubmit)
		return;

	spin_lock_irqsave(&scache_tree_lock, flags);
	cs = scache_rb_find_critical(sector_nr);
	if (cs) {
		scache_rb_del_critical(cs);
		free_critical_sector(cs);
	}
	spin_unlock_irqrestore(&scache_tree_lock, flags);
}
EXPORT_SYMBOL(scache_resubmit_sector_cleanup);

void scache_resubmit_bh_cleanup(struct buffer_head *head)
{
	sector_t sector_nr = head->b_blocknr * (head->b_size >> 9);
	struct block_device *bdev = head->b_bdev;
	dev_t bdev_nr = bdev->bd_dev;

	if (bdev != bdev->bd_contains) {
		struct hd_struct *p = bdev->bd_part;

		sector_nr += p->start_sect;
		bdev_nr = bdev->bd_contains->bd_dev;
	} else {
		scache_try_linear_remap(&bdev_nr, &sector_nr);
	}

	scache_resubmit_sector_cleanup(sector_nr);
}
EXPORT_SYMBOL(scache_resubmit_bh_cleanup);

void scache_resubmit_page_cleanup(struct page *page)
{
	if (page_has_buffers(page))
		scache_resubmit_bh_cleanup(page_buffers(page));
	else
		BUG_ON(1);
}
EXPORT_SYMBOL(scache_resubmit_page_cleanup);

bool scache_set_wait_rwsem(struct rw_semaphore *sem)
{
	bool is_critical;
	struct task_struct *waiter = current;

	spin_lock(&waiter->critical_lock);
	if (scache_is_critical_task(waiter)) {
		is_critical = true;
	} else {
		waiter->wait_rwsem = sem;
		waiter->waiting = SCACHE_WAITING_RWSEM;
		is_critical = false;
	}
	spin_unlock(&waiter->critical_lock);

	return is_critical;
}
EXPORT_SYMBOL(scache_set_wait_rwsem);

bool scache_set_wait_pglock(struct page *page)
{
	bool is_critical;
	struct task_struct *waiter = current;

	spin_lock(&waiter->critical_lock);
	if (scache_is_critical_task(waiter)) {
		is_critical = true;
	} else {
		waiter->wait_page = page;
		waiter->waiting = SCACHE_WAITING_PGLOCK;
		is_critical = false;
	}
	spin_unlock(&waiter->critical_lock);

	return is_critical;
}
EXPORT_SYMBOL(scache_set_wait_pglock);

bool scache_set_wait_bhlock(struct buffer_head *bh)
{
	bool is_critical;
	struct task_struct *waiter = current;

	spin_lock(&waiter->critical_lock);
	if (scache_is_critical_task(waiter)) {
		is_critical = true;
	} else {
		waiter->wait_bh = bh;
		waiter->waiting = SCACHE_WAITING_BHLOCK;
		is_critical = false;
	}
	spin_unlock(&waiter->critical_lock);

	return is_critical;
}
EXPORT_SYMBOL(scache_set_wait_bhlock);

bool scache_set_wait_mutex(struct mutex *lock)
{
	bool is_critical;
	struct task_struct *waiter = current;

	spin_lock(&waiter->critical_lock);
	if (scache_is_critical_task(waiter)) {
		is_critical = true;
	} else {
		waiter->wait_mutex = lock;
		waiter->waiting = SCACHE_WAITING_MUTEX;
		is_critical = false;
	}
	spin_unlock(&waiter->critical_lock);

	return is_critical;
}
EXPORT_SYMBOL(scache_set_wait_mutex);

bool scache_set_wait_task(struct task_struct *tsk)
{
	bool is_critical;
	struct task_struct *waiter = current;

	spin_lock(&waiter->critical_lock);
	if (scache_is_critical_task(waiter)) {
		is_critical = true;
	} else {
		waiter->wait_task = tsk;
		waiter->waiting = SCACHE_WAITING_TASK;
		is_critical = false;
	}
	spin_unlock(&waiter->critical_lock);

	return is_critical;
}
EXPORT_SYMBOL(scache_set_wait_task);

bool scache_set_wait_transaction(void *trx)
{
	bool is_critical;
	struct task_struct *waiter = current;

	spin_lock(&waiter->critical_lock);
	if (scache_is_critical_task(waiter)) {
		is_critical = true;
	} else {
		waiter->wait_trx = trx;
		waiter->waiting = SCACHE_WAITING_TRX;
		is_critical = false;
	}
	spin_unlock(&waiter->critical_lock);

	return is_critical;
}
EXPORT_SYMBOL(scache_set_wait_transaction);

bool scache_set_wait_sector(sector_t sector, dev_t dev)
{
	bool is_critical;
	struct task_struct *waiter = current;

	spin_lock(&waiter->critical_lock);
	if (scache_is_critical_task(waiter)) {
		is_critical = true;
	} else {
		waiter->wait_sector = sector;
		waiter->wait_dev = dev;
		waiter->waiting = SCACHE_WAITING_IO;
		is_critical = false;
	}
	spin_unlock(&waiter->critical_lock);

	return is_critical;
}
EXPORT_SYMBOL(scache_set_wait_sector);

bool scache_set_wait_bh(struct buffer_head *bh)
{
	sector_t sector_nr = bh->b_blocknr * (bh->b_size >> 9);
	struct block_device *bdev = bh->b_bdev;
	dev_t bdev_nr = bdev->bd_dev;

	if (bdev != bdev->bd_contains) {
		struct hd_struct *p = bdev->bd_part;

		sector_nr += p->start_sect;
		bdev_nr = bdev->bd_contains->bd_dev;
	} else {
		scache_try_linear_remap(&bdev_nr, &sector_nr);
	}

	return scache_set_wait_sector(sector_nr, bdev_nr);
}
EXPORT_SYMBOL(scache_set_wait_bh);

bool scache_set_wait_page(struct page *page)
{
	bool ret;

	if (page_has_buffers(page))
		ret = scache_set_wait_bh(page_buffers(page));
	else
		ret = false;

	if (!ret)
		current->wait_point = SCACHE_RESUBMIT_PGWAIT;

	return ret;
}
EXPORT_SYMBOL(scache_set_wait_page);

static void __scache_clear_wait(struct task_struct *waiter)
{
	waiter->wait_sector = 0;
	waiter->wait_dev = 0;
	waiter->wait_page = NULL;
	waiter->wait_bh = NULL;
	waiter->wait_task = NULL;
	waiter->wait_mutex = NULL;
	waiter->wait_rwsem = NULL;
	waiter->wait_trx = NULL;
	waiter->waiting = SCACHE_WAITING_NONE;
	waiter->wait_point = 0;
}
	
void scache_clear_wait(struct task_struct *waiter)
{
	spin_lock(&waiter->critical_lock);
	waiter->wait_sector = 0;
	waiter->wait_dev = 0;
	waiter->wait_page = NULL;
	waiter->wait_bh = NULL;
	waiter->wait_task = NULL;
	waiter->wait_mutex = NULL;
	waiter->wait_rwsem = NULL;
	waiter->wait_trx = NULL;
	waiter->waiting = SCACHE_WAITING_NONE;
	waiter->wait_point = 0;
	spin_unlock(&waiter->critical_lock);
}
EXPORT_SYMBOL(scache_clear_wait);

static void check_waiting_and_inherit_nested(struct task_struct *tsk, int waiting_orig)
{
	int ret;

	spin_lock(&tsk->critical_lock);
	if (tsk->waiting == SCACHE_WAITING_IO) {
		ret = scache_resubmit_sector(tsk->wait_sector, tsk->wait_dev, GFP_NOWAIT, tsk->wait_point);
		if (ret == SCACHE_RESUBMIT_MISS)
			scache_resubmit_sector_cleanup(tsk->wait_sector);
		__scache_clear_wait(tsk);
	} else if (tsk->waiting != SCACHE_WAITING_NONE) {
		printk(KERN_ERR "sCache: waiting_orig %d tsk->waiting %d\n",
				waiting_orig, tsk->waiting);
	}
	spin_unlock(&tsk->critical_lock);
}

void scache_check_waiting_and_inherit(struct task_struct *tsk)
{
	int ret;
	struct task_struct *waiter;
	transaction_t *trx;
	struct mutex *lock;
	struct buffer_head *bh;
	struct page *page;
	struct rw_semaphore *rwsem;
	unsigned long flags;

	spin_lock(&tsk->critical_lock);
	switch (tsk->waiting) {
		case SCACHE_WAITING_IO:
			ret = scache_resubmit_sector(tsk->wait_sector, tsk->wait_dev, GFP_NOWAIT, tsk->wait_point);
			if (ret == SCACHE_RESUBMIT_MISS)
				scache_resubmit_sector_cleanup(tsk->wait_sector);
			break;
		case SCACHE_WAITING_TASK:
			waiter = tsk->wait_task;
			/* Currently only jbd is the target of this criteria */
			scache_inc_and_set_critical_kernel(waiter, tsk->wait_point);
			check_waiting_and_inherit_nested(waiter, SCACHE_WAITING_TASK);
			break;
		case SCACHE_WAITING_MUTEX:
			lock = tsk->wait_mutex;
			spin_lock_irqsave(&lock->critical_lock, flags);
			lock->critical_cnt++;
			if (lock->owner && lock->inherit == false) {
				scache_inc_and_set_critical_kernel(lock->owner, SCACHE_INHERIT_MUTEX);
				lock->inherit = true;
				check_waiting_and_inherit_nested(lock->owner, SCACHE_WAITING_MUTEX);
			}
			spin_unlock_irqrestore(&lock->critical_lock, flags);
			break;
		case SCACHE_WAITING_PGLOCK:
			page = tsk->wait_page;
			spin_lock_irqsave(&page->critical_lock, flags);
			if (page->pglock_owner == tsk) {
				page->critical_cnt++;
			} else {
				page->critical_cnt++;
				if (page->pglock_owner && page->inherit == false) {
					scache_inc_and_set_critical_kernel(page->pglock_owner, SCACHE_INHERIT_PGLOCK);
					page->inherit = true;
					check_waiting_and_inherit_nested(page->pglock_owner, SCACHE_WAITING_PGLOCK);
				}
			}
			spin_unlock_irqrestore(&page->critical_lock, flags);
			break;
		case SCACHE_WAITING_BHLOCK:
			bh = tsk->wait_bh;
			spin_lock_irqsave(&bh->critical_lock, flags);
			bh->critical_cnt++;
			if (bh->bhlock_owner && bh->inherit == false) {
				if (bh->bhlock_owner == tsk) {
					printk(KERN_ERR "sCache: bh->bhlock_owner == tsk\n");
					dump_stack();
				}
				//scache_inc_and_set_critical_kernel(bh->bhlock_owner);
				bh->inherit = true;
				check_waiting_and_inherit_nested(bh->bhlock_owner, SCACHE_WAITING_BHLOCK);
			}
			spin_unlock_irqrestore(&bh->critical_lock, flags);
			break;
		case SCACHE_WAITING_RWSEM:
			rwsem = tsk->wait_rwsem;
			spin_lock(&rwsem->write_owner_lock);
			rwsem->critical_cnt++;
			if (rwsem->write_owner && rwsem->inherit == false) {
				scache_inc_and_set_critical_kernel(rwsem->write_owner, SCACHE_INHERIT_RWSEM);
				rwsem->inherit = true;
				check_waiting_and_inherit_nested(rwsem->write_owner, SCACHE_WAITING_RWSEM);
			}
			spin_unlock(&rwsem->write_owner_lock);
			break;
		case SCACHE_WAITING_TRX:
			trx = (transaction_t *) tsk->wait_trx;
			spin_lock(&trx->t_updates_list_lock);
			trx->critical = true;
			list_for_each_entry(waiter, &trx->t_updates_list, wait_task_list) {
				if (waiter->in_map_extent)
					waiter->in_map_extent = 2;
				scache_inc_and_set_critical_kernel(waiter, SCACHE_INHERIT_TRX_WAIT);
				check_waiting_and_inherit_nested(waiter, SCACHE_WAITING_TRX);
			}
			spin_unlock(&trx->t_updates_list_lock);
			break;
		case SCACHE_WAITING_NONE:
			break;
		default:
			WARN_ON(1);
			break;		
	}
	__scache_clear_wait(tsk);
	spin_unlock(&tsk->critical_lock);
}

void scache_init_mem(void)
{
	cache_critical_task = 0;
	cache_writesync = 0;
	critical_inherit_kernel = 0;
	critical_inherit_user = 0;
	enable_resubmit = 0;

	reset_all = 0;
	enable_stable_page = 0;
	enable_clflush = 0;
	enable_nts = 0;
	enable_ntl = 0;
	cancel_destaging = 0;
	write_delay = 0;
	read_delay = 0;

	atomic_set(&critical_sector_cnt, 0);
}

#ifdef CONFIG_SYSFS

#define SCACHE_ATTR_RO(_name) \
	static struct kobj_attribute _name##_attr = __ATTR_RO(_name)
#define SCACHE_ATTR(_name) \
	static struct kobj_attribute _name##_attr = \
__ATTR(_name, 0644, _name##_show, _name##_store)

static ssize_t cache_critical_task_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", cache_critical_task);
}

static ssize_t cache_critical_task_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;

	if (cache_critical_task != flags)
		cache_critical_task = flags;

	return count;
}
SCACHE_ATTR(cache_critical_task);

static ssize_t critical_inherit_user_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", critical_inherit_user);
}

static ssize_t critical_inherit_user_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;
	if (flags == 1 && !cache_critical_task)
		return -EINVAL;

	if (critical_inherit_user != flags)
		critical_inherit_user = flags;

	return count;
}
SCACHE_ATTR(critical_inherit_user);

static ssize_t critical_inherit_kernel_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", critical_inherit_kernel);
}

static ssize_t critical_inherit_kernel_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;
	if (flags == 1 && !cache_critical_task)
		return -EINVAL;

	if (critical_inherit_kernel != flags)
		critical_inherit_kernel = flags;

	return count;
}
SCACHE_ATTR(critical_inherit_kernel);

static ssize_t enable_resubmit_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", enable_resubmit);
}

static ssize_t enable_resubmit_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;

	if (enable_resubmit != flags)
		enable_resubmit = flags;

	return count;
}
SCACHE_ATTR(enable_resubmit);

static ssize_t cancel_destaging_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", cancel_destaging);
}

static ssize_t cancel_destaging_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;

	if (cancel_destaging != flags)
		cancel_destaging = flags;

	return count;
}
SCACHE_ATTR(cancel_destaging);

static ssize_t reset_all_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", reset_all);
}

static ssize_t reset_all_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags != 1)
		return -EINVAL;

	scache_init_mem();

	return count;
}
SCACHE_ATTR(reset_all);

static ssize_t enable_stable_page_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", enable_stable_page);
}

static ssize_t enable_stable_page_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;

	if (enable_stable_page != flags)
		enable_stable_page = flags;

	return count;
}
SCACHE_ATTR(enable_stable_page);

static ssize_t enable_clflush_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", enable_clflush);
}

static ssize_t enable_clflush_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;

	if (enable_clflush != flags)
		enable_clflush = flags;

	return count;
}
SCACHE_ATTR(enable_clflush);

static ssize_t enable_nts_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", enable_nts);
}

static ssize_t enable_nts_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;

	if (enable_nts != flags)
		enable_nts = flags;

	return count;
}
SCACHE_ATTR(enable_nts);

static ssize_t enable_ntl_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", enable_ntl);
}

static ssize_t enable_ntl_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;

	if (enable_ntl != flags)
		enable_ntl = flags;

	return count;
}
SCACHE_ATTR(enable_ntl);

static ssize_t write_delay_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%lu\n", write_delay);
}

static ssize_t write_delay_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;

	if (write_delay != flags)
		write_delay = flags;

	return count;
}
SCACHE_ATTR(write_delay);

static ssize_t read_delay_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%lu\n", read_delay);
}

static ssize_t read_delay_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;

	if (read_delay != flags)
		read_delay = flags;

	return count;
}
SCACHE_ATTR(read_delay);

static ssize_t cache_writesync_show(struct kobject *kobj,
		struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", cache_writesync);
}

static ssize_t cache_writesync_store(struct kobject *kobj,
		struct kobj_attribute *attr, const char *buf, size_t count)
{
	unsigned long flags;
	int err;

	err = strict_strtoul(buf, 10, &flags);
	if (err || flags > UINT_MAX)
		return -EINVAL;
	if (flags > 1 || flags < 0)
		return -EINVAL;

	if (cache_writesync != flags)
		cache_writesync = flags;

	return count;
}
SCACHE_ATTR(cache_writesync);

static struct attribute *scache_attrs[] = {
	&cache_critical_task_attr.attr,
	&cache_writesync_attr.attr,
	&critical_inherit_user_attr.attr,
	&critical_inherit_kernel_attr.attr,
	&enable_resubmit_attr.attr,
	&reset_all_attr.attr,
	&enable_stable_page_attr.attr,
	&enable_clflush_attr.attr,
	&enable_nts_attr.attr,
	&enable_ntl_attr.attr,
	&cancel_destaging_attr.attr,
	&write_delay_attr.attr,
	&read_delay_attr.attr,
	NULL,
};

static struct attribute_group scache_attr_group = {
	.attrs = scache_attrs,
	.name = "scache",
};

#endif /* CONFIG_SYSFS */

static int __init scache_init(void)
{
	int err;

	err = scache_mempool_init();
	if (err)
		goto out;

	scache_init_mem();

	spin_lock_init(&scache_tree_lock);

#ifdef CONFIG_SYSFS
	err = sysfs_create_group(mm_kobj, &scache_attr_group);
	if (err) {
		printk(KERN_ERR "sCache: register sysfs failed\n");
		goto out;
	}
#endif

	return 0;

out:
	scache_mempool_free();

	return err;
}
module_init(scache_init)

