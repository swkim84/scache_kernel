#undef TRACE_SYSTEM
#define TRACE_SYSTEM flashcache

#if !defined(_TRACE_FLASHCACHE_H) || defined(TRACE_HEADER_MULTI_READ)
#define _TRACE_FLASHCACHE_H

#include <linux/blktrace_api.h>
#include <linux/blkdev.h>
#include <linux/tracepoint.h>

#define RWBS_LEN	8

#define CACHE_STATE_MASK 0x7f

TRACE_EVENT(flashcache_uncached_write,

	TP_PROTO(struct bio *bio, struct inode *inode, struct dentry *dentry),

	TP_ARGS(bio, inode, dentry),

	TP_STRUCT__entry(
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
		__field( unsigned long,	i_ino		)
		__array( char,	dname,	DNAME_INLINE_LEN)
		__field( unsigned int,	critical_flags	)
	),

	TP_fast_assign(
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
		__entry->i_ino		= inode ? inode->i_ino : 0;
		__entry->critical_flags = current->critical_flags;
	),

	TP_printk("[%s %s %s] sector %lu size %u rwbs %s i_ino %lu\n",
		__entry->critical_flags & SCACHE_CRITICAL ? "CP" : "",
		__entry->critical_flags & SCACHE_CRITICAL_INHERIT_KERNEL ? "CIK" : "",
		__entry->critical_flags & SCACHE_CRITICAL_INHERIT_USER ? "CIU" : "",
		__entry->sector, __entry->size, __entry->rwbs, __entry->i_ino)
);

TRACE_EVENT(flashcache_map,

	TP_PROTO(struct bio *bio, struct inode *inode, struct dentry *dentry),

	TP_ARGS(bio, inode, dentry),

	TP_STRUCT__entry(
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
		__field( unsigned long,	i_ino		)
		__array( char,	dname,	DNAME_INLINE_LEN)
		__field( unsigned int,	critical_flags	)
	),

	TP_fast_assign(
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
		__entry->i_ino		= inode ? inode->i_ino : 0;
		__entry->critical_flags = current->critical_flags;
	),

	TP_printk("[%s %s %s] sector %lu size %u rwbs %s i_ino %lu\n",
		__entry->critical_flags & SCACHE_CRITICAL ? "CP" : "",
		__entry->critical_flags & SCACHE_CRITICAL_INHERIT_KERNEL ? "CIK" : "",
		__entry->critical_flags & SCACHE_CRITICAL_INHERIT_USER ? "CIU" : "",
		__entry->sector, __entry->size, __entry->rwbs, __entry->i_ino)
);


TRACE_EVENT(flashcache_write,

	TP_PROTO(struct bio *bio, struct inode *inode, struct dentry *dentry),

	TP_ARGS(bio, inode, dentry),

	TP_STRUCT__entry(
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
		__field( unsigned long,	i_ino		)
		__array( char,	dname,	DNAME_INLINE_LEN)
		__field( unsigned int,	critical_flags	)
	),

	TP_fast_assign(
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
		__entry->i_ino		= inode ? inode->i_ino : 0;
		__entry->critical_flags = current->critical_flags;
	),

	TP_printk("[%s %s %s] sector %lu size %u rwbs %s i_ino %lu\n",
		__entry->critical_flags & SCACHE_CRITICAL ? "CP" : "",
		__entry->critical_flags & SCACHE_CRITICAL_INHERIT_KERNEL ? "CIK" : "",
		__entry->critical_flags & SCACHE_CRITICAL_INHERIT_USER ? "CIU" : "",
		__entry->sector, __entry->size, __entry->rwbs, __entry->i_ino)
);

TRACE_EVENT(flashcache_resubmit_bio,

	TP_PROTO(struct bio *bio, struct inode *inode, struct dentry *dentry),

	TP_ARGS(bio, inode, dentry),

	TP_STRUCT__entry(
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
		__field( unsigned long,	i_ino		)
		__array( char,	dname,	DNAME_INLINE_LEN)
		__field( unsigned int,	critical_flags	)
	),

	TP_fast_assign(
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
		__entry->i_ino		= inode ? inode->i_ino : 0;
		__entry->critical_flags = current->critical_flags;
	),

	TP_printk("[%s %s %s] sector %lu size %u rwbs %s i_ino %lu\n",
		__entry->critical_flags & SCACHE_CRITICAL ? "CP" : "",
		__entry->critical_flags & SCACHE_CRITICAL_INHERIT_KERNEL ? "CIK" : "",
		__entry->critical_flags & SCACHE_CRITICAL_INHERIT_USER ? "CIU" : "",
		__entry->sector, __entry->size, __entry->rwbs, __entry->i_ino)
);

/*
TRACE_EVENT(flashcache_inval_blocks,

	TP_PROTO(int queued, struct bio *bio),

	TP_ARGS(queued, bio),

	TP_STRUCT__entry(
		__field( int,		queued		)
		__field( bool,		dm_critical	)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
		__field( struct page*,	bv_page		)
	),

	TP_fast_assign(
		__entry->queued		= queued;
		__entry->dm_critical	= bio->dm_critical;
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
		__entry->bv_page	= (bio->bi_io_vec[0]).bv_page;
	),

	TP_printk("[%s] queued=%d sector=%lu size=%u rwbs=%s pfn=%lu",
		__entry->dm_critical ? "CRITICAL" : "INCRITICAL",
		__entry->queued, __entry->sector, __entry->size, __entry->rwbs, 
		__entry->bv_page ? page_to_pfn(__entry->bv_page) : 0)
);

TRACE_EVENT(flashcache_write_hit,

	TP_PROTO(struct cacheblock *cacheblk, struct bio *bio),

	TP_ARGS(cacheblk, bio),

	TP_STRUCT__entry(
		__field( u_int16_t,	cache_state	)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	dbn		)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
		__field( struct page*,	bv_page		)
	),

	TP_fast_assign(
		__entry->cache_state	= cacheblk->cache_state;
		__entry->nr_queued	= cacheblk->nr_queued;
		__entry->dbn		= cacheblk->dbn;
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
		__entry->bv_page	= (bio->bi_io_vec[0]).bv_page;
	),

	TP_printk("[%s] cache_state=%x dbn=%lu sector=%lu size=%u rwbs=%s pfn=%lu nr_queued=%d",
		(!(__entry->cache_state & BLOCK_IO_INPROG) && (__entry->nr_queued == 0)) ?  "issue" : "pending",
		__entry->cache_state & CACHE_STATE_MASK, __entry->dbn, __entry->sector, __entry->size,
		__entry->rwbs, __entry->bv_page ? page_to_pfn(__entry->bv_page) : 0, __entry->nr_queued)
);

TRACE_EVENT(flashcache_writeback_cancelled,

	TP_PROTO(int ret, struct cacheblock *cacheblk, struct bio *bio),

	TP_ARGS(ret, cacheblk, bio),

	TP_STRUCT__entry(
		__field( int,		ret	)
		__field( u_int16_t,	cache_state	)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	dbn		)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
		__field( struct page*,	bv_page		)
	),

	TP_fast_assign(
		__entry->ret		= ret;
		__entry->cache_state	= cacheblk->cache_state;
		__entry->nr_queued	= cacheblk->nr_queued;
		__entry->dbn		= cacheblk->dbn;
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
		__entry->bv_page	= (bio->bi_io_vec[0]).bv_page;
	),

	TP_printk("[%s] cache_state=%x dbn=%lu sector=%lu size=%u rwbs=%s pfn=%lu nr_queued=%d ret=%d",
		(!(__entry->cache_state & BLOCK_IO_INPROG) && (__entry->nr_queued == 0)) ?  "issue" : "pending",
		__entry->cache_state & CACHE_STATE_MASK, __entry->dbn, __entry->sector, __entry->size,
		__entry->rwbs, __entry->bv_page ? page_to_pfn(__entry->bv_page) : 0, __entry->nr_queued, __entry->ret)
);

TRACE_EVENT(flashcache_write_miss,

	TP_PROTO(int queued, struct cacheblock *cacheblk, struct bio *bio),

	TP_ARGS(queued, cacheblk, bio),

	TP_STRUCT__entry(
		__field( int,		queued		)
		__field( u_int16_t,	cache_state	)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	dbn		)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
		__field( struct page*,	bv_page		)
	),

	TP_fast_assign(
		__entry->queued		= queued;
		__entry->cache_state	= cacheblk->cache_state;
		__entry->nr_queued	= cacheblk->nr_queued;
		__entry->dbn		= cacheblk->dbn;
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
		__entry->bv_page	= (bio->bi_io_vec[0]).bv_page;
	),

	TP_printk("[%s] cache_state=%x dbn=%lu sector=%lu size=%u rwbs=%s pfn=%lu nr_queued=%d",
		!__entry->queued  ?  "issue" : "queued",
		__entry->cache_state & CACHE_STATE_MASK, __entry->dbn, __entry->sector, __entry->size,
		__entry->rwbs, __entry->bv_page ? page_to_pfn(__entry->bv_page) : 0, __entry->nr_queued)

);

TRACE_EVENT(flashcache_md_write_kickoff,

	TP_PROTO(int action, struct cacheblock *cacheblk),

	TP_ARGS(action, cacheblk),

	TP_STRUCT__entry(
		__field( int,		action		)
		__field( u_int16_t,	cache_state	)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	dbn		)
	),

	TP_fast_assign(
		__entry->action		= action;
		__entry->cache_state	= cacheblk->cache_state;
		__entry->nr_queued	= cacheblk->nr_queued;
		__entry->dbn		= cacheblk->dbn;
	),

	TP_printk("[%s] cache_state=%x nr_queued=%d dbn=%lu",
		__entry->action == WRITEDISK  ?  "WRITEDISK" : 
		 (__entry->action == WRITECACHE ? "WRITECACHE" :
		 (__entry->action == WRITEDISK_SYNC ? "WRITEDISK_SYNC" : "")),
		__entry->cache_state & CACHE_STATE_MASK, __entry->nr_queued, __entry->dbn)
);

TRACE_EVENT(flashcache_md_write_done,

	TP_PROTO(int action, struct cacheblock *cacheblk),

	TP_ARGS(action, cacheblk),

	TP_STRUCT__entry(
		__field( int,		action		)
		__field( u_int16_t,	cache_state	)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	dbn		)
	),

	TP_fast_assign(
		__entry->action		= action;
		__entry->cache_state	= cacheblk->cache_state;
		__entry->nr_queued	= cacheblk->nr_queued;
		__entry->dbn		= cacheblk->dbn;
	),

	TP_printk("[%s] cache_state=%x nr_queued=%d dbn=%lu",
		__entry->action == WRITEDISK  ?  "WRITEDISK" : 
		 (__entry->action == WRITECACHE ? "WRITECACHE" :
		 (__entry->action == WRITEDISK_SYNC ? "WRITEDISK_SYNC" : "")),
		__entry->cache_state & CACHE_STATE_MASK, __entry->nr_queued, __entry->dbn)
);

TRACE_EVENT(flashcache_dirty_writeback,

	TP_PROTO(struct cacheblock *cacheblk),

	TP_ARGS(cacheblk),

	TP_STRUCT__entry(
		__field( u_int16_t,	cache_state	)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	dbn		)
	),

	TP_fast_assign(
		__entry->cache_state	= cacheblk->cache_state;
		__entry->nr_queued	= cacheblk->nr_queued;
		__entry->dbn		= cacheblk->dbn;
	),

	TP_printk("cache_state=%x nr_queued=%d dbn=%lu",
		__entry->cache_state & CACHE_STATE_MASK, __entry->nr_queued, __entry->dbn)
);

TRACE_EVENT(flashcache_kcopyd_callback,

	TP_PROTO(struct cacheblock *cacheblk),

	TP_ARGS(cacheblk),

	TP_STRUCT__entry(
		__field( u_int16_t,	cache_state	)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	dbn		)
	),

	TP_fast_assign(
		__entry->cache_state	= cacheblk->cache_state;
		__entry->nr_queued	= cacheblk->nr_queued;
		__entry->dbn		= cacheblk->dbn;
	),

	TP_printk("cache_state=%x nr_queued=%d dbn=%lu",
		__entry->cache_state & CACHE_STATE_MASK, __entry->nr_queued, __entry->dbn)
);

TRACE_EVENT(flashcache_do_pending_noerror,

	TP_PROTO(int action, struct cacheblock *cacheblk),

	TP_ARGS(action, cacheblk),

	TP_STRUCT__entry(
		__field( int,		action		)
		__field( u_int16_t,	cache_state	)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	dbn		)
	),

	TP_fast_assign(
		__entry->action		= action;
		__entry->cache_state	= cacheblk->cache_state;
		__entry->nr_queued	= cacheblk->nr_queued;
		__entry->dbn		= cacheblk->dbn;
	),

	TP_printk("[%s] cache_state=%x nr_queued=%d dbn=%lu",
		__entry->action == READCACHE ? "READCACHE" : 
		(__entry->action == WRITECACHE ? "WRITECACHE" :
		(__entry->action == INVALIDATE ? "INVALIDATE" : "")),
		__entry->cache_state & CACHE_STATE_MASK, 
		__entry->nr_queued, __entry->dbn)
);

TRACE_EVENT(flashcache_start_uncached_io,

	TP_PROTO(int rw, struct bio *bio),

	TP_ARGS(rw, bio),

	TP_STRUCT__entry(
		__field( int,		rw		)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
	),

	TP_fast_assign(
		__entry->rw		= rw;
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
	),

	TP_printk("[%s] sector=%lu size=%u rwbs=%s",
		__entry->rw ?  "WRITE" : "READ",
		__entry->sector, __entry->size, __entry->rwbs)

);

TRACE_EVENT(flashcache_uncached_io_complete,

	TP_PROTO(int queued, struct bio *bio),

	TP_ARGS(queued, bio),

	TP_STRUCT__entry(
		__field( int,		queued		)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
	),

	TP_fast_assign(
		__entry->queued		= queued;
		__entry->sector		= bio->bi_sector;
		__entry->size		= bio->bi_size;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
	),

	TP_printk("[%s] sector=%lu size=%u rwbs=%s",
		__entry->queued ?  (__entry->queued < 0 ? "error" : "re-exec") : "endio",
		__entry->sector, __entry->size, __entry->rwbs)

);

TRACE_EVENT(flashcache_do_io,

	TP_PROTO(struct bio *bio),

	TP_ARGS(bio),

	TP_STRUCT__entry(
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
	),

	TP_fast_assign(
		__entry->sector		= bio ? bio->bi_sector : 0;
		__entry->size		= bio ? bio->bi_size : 0;
	),

	TP_printk("sector=%lu size=%u",
		__entry->sector, __entry->size)

);

TRACE_EVENT(flashcache_io_callback,

	TP_PROTO(int action, struct cacheblock *cacheblk, struct bio *bio),

	TP_ARGS(action, cacheblk, bio),

	TP_STRUCT__entry(
		__field( int,		action		)
		__field( u_int16_t,	cache_state	)
		__field( sector_t,	dbn		)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
	),

	TP_fast_assign(
		__entry->action		= action;
		__entry->cache_state	= cacheblk ? cacheblk->cache_state : 0;
		__entry->dbn		= cacheblk ? cacheblk->dbn : 0;
		__entry->nr_queued	= cacheblk ? cacheblk->nr_queued : -1;
		__entry->sector		= bio ? bio->bi_sector : 0;
		__entry->size		= bio ? bio->bi_size : 0;
	),

	TP_printk("[%s] cache_state=%x dbn=%lu nr_queued=%d sector=%lu size=%u",
		__entry->action == READDISK  ?  "READDISK" : 
		(__entry->action == READCACHE ? "READCACHE" : 
		(__entry->action == READFILL ? "READFILL" : 
		 (__entry->action == WRITECACHE ? "WRITECACHE" : ""))),
		__entry->cache_state & CACHE_STATE_MASK, __entry->dbn,
		__entry->nr_queued, __entry->sector, __entry->size)
);

TRACE_EVENT(flashcache_md_write,

	TP_PROTO(u_int32_t nr_in_prog, struct cacheblock *cacheblk, struct bio *bio),

	TP_ARGS(nr_in_prog, cacheblk, bio),

	TP_STRUCT__entry(
		__field( u_int32_t,	nr_in_prog	)
		__field( u_int16_t,	cache_state	)
		__field( sector_t,	dbn		)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
	),

	TP_fast_assign(
		__entry->nr_in_prog	= nr_in_prog;
		__entry->cache_state	= cacheblk ? cacheblk->cache_state : 0;
		__entry->dbn		= cacheblk ? cacheblk->dbn : 0;
		__entry->nr_queued	= cacheblk ? cacheblk->nr_queued : -1;
		__entry->sector		= bio ? bio->bi_sector : 0;
		__entry->size		= bio ? bio->bi_size : 0;
	),

	TP_printk("[%s] cache_state=%x dbn=%lu nr_queued=%d sector=%lu size=%u",
		__entry->nr_in_prog  ?  "md write queued" : "md write scheduled", 
		__entry->cache_state & CACHE_STATE_MASK, __entry->dbn,
		__entry->nr_queued, __entry->sector, __entry->size)
);

TRACE_EVENT(flashcache_do_pending_error,

	TP_PROTO(struct cacheblock *cacheblk, struct bio *bio, int error, int action),

	TP_ARGS(cacheblk, bio, error, action),

	TP_STRUCT__entry(
		__field( int,		action		)
		__field( u_int16_t,	cache_state	)
		__field( sector_t,	dbn		)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
	),

	TP_fast_assign(
		__entry->action		= action;
		__entry->cache_state	= cacheblk ? cacheblk->cache_state : 0;
		__entry->dbn		= cacheblk ? cacheblk->dbn : 0;
		__entry->nr_queued	= cacheblk ? cacheblk->nr_queued : -1;
		__entry->sector		= bio ? bio->bi_sector : 0;
		__entry->size		= bio ? bio->bi_size : 0;
	),

	TP_printk("[%s] cache_state=%x dbn=%lu nr_queued=%d sector=%lu size=%u",
		__entry->action == READDISK  ?  "READDISK" : 
		(__entry->action == READCACHE ? "READCACHE" : 
		(__entry->action == READFILL ? "READFILL" : 
		 (__entry->action == WRITECACHE ? "WRITECACHE" : ""))),
		__entry->cache_state & CACHE_STATE_MASK, __entry->dbn,
		__entry->nr_queued, __entry->sector, __entry->size)
);

TRACE_EVENT(flashcache_do_pending_error_exit,

	TP_PROTO(struct cacheblock *cacheblk, struct bio *bio, int error, int action),

	TP_ARGS(cacheblk, bio, error, action),

	TP_STRUCT__entry(
		__field( int,		action		)
		__field( u_int16_t,	cache_state	)
		__field( sector_t,	dbn		)
		__field( int16_t,	nr_queued	)
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
	),

	TP_fast_assign(
		__entry->action		= action;
		__entry->cache_state	= cacheblk ? cacheblk->cache_state : 0;
		__entry->dbn		= cacheblk ? cacheblk->dbn : 0;
		__entry->nr_queued	= cacheblk ? cacheblk->nr_queued : -1;
		__entry->sector		= bio ? bio->bi_sector : 0;
		__entry->size		= bio ? bio->bi_size : 0;
	),

	TP_printk("[%s] cache_state=%x dbn=%lu nr_queued=%d sector=%lu size=%u",
		__entry->action == READDISK  ?  "READDISK" : 
		(__entry->action == READCACHE ? "READCACHE" : 
		(__entry->action == READFILL ? "READFILL" : 
		 (__entry->action == WRITECACHE ? "WRITECACHE" : ""))),
		__entry->cache_state & CACHE_STATE_MASK, __entry->dbn,
		__entry->nr_queued, __entry->sector, __entry->size)
);

TRACE_EVENT(flashcache_bio_requeue,

	TP_PROTO(struct bio *bio),

	TP_ARGS(bio),

	TP_STRUCT__entry(
		__field( sector_t,	sector		)
		__field( unsigned int,	size		)
		__array( char,		rwbs,	RWBS_LEN)
	),

	TP_fast_assign(
		__entry->sector		= bio ? bio->bi_sector : 0;
		__entry->size		= bio ? bio->bi_size : 0;
		blk_fill_rwbs(__entry->rwbs, bio->bi_rw, bio->bi_size);
	),

	TP_printk("sector=%lu size=%u rwbs=%s",
		__entry->sector, __entry->size, __entry->rwbs)
);
*/

#endif /* _TRACE_FLASHCACHE_H */

/* This part must be outside protection */
#include <trace/define_trace.h>

