#ifndef _LINUX_SCACHE_H
#define _LINUX_SCACHE_H

#define SCACHE_CRITICAL                 0x00000001
#define SCACHE_CRITICAL_INHERIT_KERNEL  0x00000002
#define SCACHE_CRITICAL_INHERIT_USER    0x00000004
#define SCACHE_CRITICAL_INHERIT         (SCACHE_CRITICAL_INHERIT_KERNEL | SCACHE_CRITICAL_INHERIT_USER)
#define SCACHE_CRITICAL_MASK            (SCACHE_CRITICAL | SCACHE_CRITICAL_INHERIT)

/* Resubmit status */
#define SCACHE_RESUBMIT_MISS		0
#define SCACHE_RESUBMIT_HIT			1
#define SCACHE_RESUBMIT_FAILED		2

/* Resubmit point */
#define SCACHE_RESUBMIT_BHLOCK		1
#define SCACHE_RESUBMIT_BHWAIT		2
#define SCACHE_RESUBMIT_PGLOCK		3
#define SCACHE_RESUBMIT_PGWAIT		4
#define SCACHE_RESUBMIT_SHADOW		5

/* Criticality inheritance point */
#define SCACHE_INHERIT_MUTEX		0x1
#define SCACHE_INHERIT_PGLOCK		0x2
#define SCACHE_INHERIT_RWSEM		0x4
#define SCACHE_INHERIT_TRX_WAIT		0x10
#define SCACHE_INHERIT_TRX_COMMIT	0x20
#define SCACHE_INHERIT_TRX_LOCKED	0x40
#define SCACHE_INHERIT_USER			0x80

/* Task's waiting state */
#define SCACHE_WAITING_NONE			0
#define SCACHE_WAITING_IO			1
#define SCACHE_WAITING_TASK			2
#define SCACHE_WAITING_BHLOCK		3
#define SCACHE_WAITING_PGLOCK		4
#define SCACHE_WAITING_RWSEM		5
#define SCACHE_WAITING_MUTEX		6
#define SCACHE_WAITING_TRX			7

struct critical_sector {
	struct rb_node  rb_node;
	sector_t        sector;
};

struct task_struct;
struct bio;
struct dm_target;
struct buffer_head;
struct block_device;
struct rw_semaphore;

extern spinlock_t scache_tree_lock;

void scache_nts_memcpy(void* to, const void* from, size_t size);
void scache_ntl_memcpy(void* to, void* from, size_t size);

int scache_set_critical_flags(struct task_struct *tsk, int val);
inline void scache_inc_and_set_critical_kernel(struct task_struct *tsk, int inherit_point);
inline void scache_inc_and_set_critical_user(struct task_struct *tsk);
inline void scache_dec_and_clear_critical_kernel(struct task_struct *tsk, int inherit_point);
inline void scache_dec_and_clear_critical_user(struct task_struct *tsk);

inline int scache_stable_page_enabled(void);
inline int scache_clflush_enabled(void);
inline int scache_nts_enabled(void);
inline int scache_ntl_enabled(void);
inline int scache_resubmit_enabled(void);
inline int scache_critical_task_enabled(void);
inline int scache_cache_writesync_enabled(void);
inline unsigned long scache_cancel_destaging(void);

inline unsigned long scache_write_delay(void);
inline unsigned long scache_read_delay(void);

int scache_resubmit_bh(struct buffer_head *, gfp_t gfp_mask, int resubmit_point);
int scache_resubmit_sector(sector_t sector_nr, dev_t bdev, gfp_t gfp_mask, int resubmit_point);
int scache_resubmit_page(struct page *, gfp_t gfp_mask, int resubmit_point);
void scache_resubmit_bh_cleanup(struct buffer_head *);
void scache_resubmit_sector_cleanup(sector_t sector_nr);
void scache_resubmit_page_cleanup(struct page *);

void scache_check_waiting_and_inherit(struct task_struct *tsk);

sector_t scache_get_remapped_sector_reverse(struct bio *bio, struct block_device *bdev);

int scache_rb_add(struct bio *);
void scache_rb_del(struct bio *);
struct bio *scache_rb_find(sector_t sector_nr);
int scache_rb_add_critical(struct critical_sector *cs);
void scache_rb_del_critical(struct critical_sector *cs);
struct critical_sector *scache_rb_find_critical(sector_t sector_nr);

inline struct critical_sector *alloc_critical_sector(gfp_t gfp_mask);
inline void free_critical_sector(struct critical_sector *cs);

inline void dm_alloc_src_bdev_from_target(struct dm_target *, struct block_device *);
int is_dm_md(dev_t dev);

int scache_is_critical_task(struct task_struct *tsk);
int scache_is_critical_sector(struct bio *bio);

bool scache_set_wait_rwsem(struct rw_semaphore *rwsem);
bool scache_set_wait_pglock(struct page *page);
bool scache_set_wait_bhlock(struct buffer_head *bh);
bool scache_set_wait_mutex(struct mutex *lock);
bool scache_set_wait_task(struct task_struct *tsk);
bool scache_set_wait_sector(sector_t sector, dev_t dev);
bool scache_set_wait_bh(struct buffer_head *bh);
bool scache_set_wait_page(struct page *page);
bool scache_set_wait_transaction(void *trx);
void scache_clear_wait(struct task_struct *waiter);

void scache_try_linear_remap(dev_t *bdev_nr, sector_t *sector_nr);

#endif
